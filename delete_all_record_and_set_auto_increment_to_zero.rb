models.map(&:table_name).each do |tn|
  ActiveRecord::Base.connection.execute("DELETE FROM #{tn}")
  ActiveRecord::Base.connection.execute("ALTER TABLE #{tn} AUTO_INCREMENT = 0")
end